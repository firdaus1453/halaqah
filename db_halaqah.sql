-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 27, 2019 at 04:01 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_halaqah`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_kelas`
--

CREATE TABLE `tb_kelas` (
  `id_kelas` int(90) NOT NULL,
  `nama_kelas` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_kelas`
--

INSERT INTO `tb_kelas` (`id_kelas`, `nama_kelas`) VALUES
(1, 'X RPL A'),
(2, 'X RPL B'),
(3, 'X RPL C'),
(4, 'X RPL D');

-- --------------------------------------------------------

--
-- Table structure for table `tb_manzil`
--

CREATE TABLE `tb_manzil` (
  `id_manzil` int(90) NOT NULL,
  `id_setoran` int(90) NOT NULL,
  `surat_manzil` varchar(90) NOT NULL,
  `ayat_manzil` varchar(90) NOT NULL,
  `nilai_manzil` varchar(1) NOT NULL,
  `baris_manzil` int(90) NOT NULL,
  `absen_manzil` varchar(1) NOT NULL,
  `keterangan_manzil` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_sabaq`
--

CREATE TABLE `tb_sabaq` (
  `id_sabaq` int(90) NOT NULL,
  `id_setoran` int(90) NOT NULL,
  `surat_sabaq` varchar(90) NOT NULL,
  `ayat_sabaq` varchar(90) NOT NULL,
  `nilai_sabaq` varchar(1) NOT NULL,
  `baris_sabaq` int(90) NOT NULL,
  `absen_sabaq` varchar(1) NOT NULL,
  `keterangan_sabaq` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_sabaq`
--

INSERT INTO `tb_sabaq` (`id_sabaq`, `id_setoran`, `surat_sabaq`, `ayat_sabaq`, `nilai_sabaq`, `baris_sabaq`, `absen_sabaq`, `keterangan_sabaq`) VALUES
(1, 1, 'Al-Ikhlas', '1-2', 'A', 3, 'h', 'Bagus');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sabqi`
--

CREATE TABLE `tb_sabqi` (
  `id_sabqi` int(90) NOT NULL,
  `id_setoran` int(90) NOT NULL,
  `surat_sabqi` varchar(90) NOT NULL,
  `ayat_sabqi` varchar(90) NOT NULL,
  `nilai_sabqi` varchar(1) NOT NULL,
  `baris_sabqi` int(90) NOT NULL,
  `absen_sabqi` varchar(1) NOT NULL,
  `keterangan_sabqi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_setoran`
--

CREATE TABLE `tb_setoran` (
  `id_setoran` int(90) NOT NULL,
  `tanggal_setoran` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_setoran`
--

INSERT INTO `tb_setoran` (`id_setoran`, `tanggal_setoran`) VALUES
(1, '2019-02-17'),
(2, '2019-02-18'),
(3, '2019-02-19'),
(4, '2019-02-20');

-- --------------------------------------------------------

--
-- Table structure for table `tb_siswa`
--

CREATE TABLE `tb_siswa` (
  `id_siswa` int(90) NOT NULL,
  `id_user` int(90) NOT NULL,
  `id_kelas` int(90) NOT NULL,
  `nis` int(90) NOT NULL,
  `nama_siswa` varchar(90) NOT NULL,
  `password` varchar(50) NOT NULL,
  `tahun_ajaran` varchar(30) NOT NULL,
  `is_absen` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_siswa`
--

INSERT INTO `tb_siswa` (`id_siswa`, `id_user`, `id_kelas`, `nis`, `nama_siswa`, `password`, `tahun_ajaran`, `is_absen`) VALUES
(2, 5, 3, 181903025, 'Daffa Azzaqihaq Soemarta', '123', '2018/2019', 0),
(3, 5, 3, 181903026, 'Fikriansyah Al Anshari', '123', '2018/2019', 0),
(4, 5, 3, 1234521, 'Morer', '202cb962ac59075b964b07152d234b70', '2018/2019', 0),
(7, 2, 2, 123123, 'Tes update2', '81dc9bdb52d04dc20036dbd8313ed055', '2018/2020', 1),
(8, 2, 2, 123123, 'Tes update2', '81dc9bdb52d04dc20036dbd8313ed055', '2018/2020', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `id_kelas` int(90) NOT NULL,
  `username` varchar(90) NOT NULL,
  `password` varchar(90) NOT NULL,
  `nama_guru` varchar(90) NOT NULL,
  `tahun` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `id_kelas`, `username`, `password`, `nama_guru`, `tahun`) VALUES
(1, 3, 'admin', 'admin', 'Firdaus', '2018/2019'),
(2, 3, 'tesaja', '202cb962ac59075b964b07152d234b70', 'Firdaus tes', '2018/2019'),
(5, 3, 'firdaus', '202cb962ac59075b964b07152d234b70', 'Muhammad Firdaus', '2018/2019');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `tb_manzil`
--
ALTER TABLE `tb_manzil`
  ADD PRIMARY KEY (`id_manzil`);

--
-- Indexes for table `tb_sabaq`
--
ALTER TABLE `tb_sabaq`
  ADD PRIMARY KEY (`id_sabaq`);

--
-- Indexes for table `tb_sabqi`
--
ALTER TABLE `tb_sabqi`
  ADD PRIMARY KEY (`id_sabqi`);

--
-- Indexes for table `tb_setoran`
--
ALTER TABLE `tb_setoran`
  ADD PRIMARY KEY (`id_setoran`);

--
-- Indexes for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_kelas`
--
ALTER TABLE `tb_kelas`
  MODIFY `id_kelas` int(90) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_manzil`
--
ALTER TABLE `tb_manzil`
  MODIFY `id_manzil` int(90) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_sabaq`
--
ALTER TABLE `tb_sabaq`
  MODIFY `id_sabaq` int(90) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_sabqi`
--
ALTER TABLE `tb_sabqi`
  MODIFY `id_sabqi` int(90) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tb_setoran`
--
ALTER TABLE `tb_setoran`
  MODIFY `id_setoran` int(90) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_siswa`
--
ALTER TABLE `tb_siswa`
  MODIFY `id_siswa` int(90) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
