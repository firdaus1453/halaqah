<?php 
include './config/koneksi.php';
	//response array 
	$response = array(); 
	
	
	if($_SERVER['REQUEST_METHOD']=='POST'){

		//checking the required parameters from the request 
		if(
			isset($_POST['id_user']) and isset($_POST['id_kelas']) and isset($_POST['nis']) 
		) {
		
		$iduser = $_POST['id_user'];
		$idkelas = $_POST['id_kelas'];
		$nis = $_POST['nis'];
		$namasiswa = $_POST['nama_siswa'];
		$password = md5($_POST["password"]);
		$tahunajaran = $_POST['tahun_ajaran'];
		$isabsen = $_POST["is_absen"];

		$getnis = mysqli_query($connection, "SELECT * FROM tb_siswa siswa,tb_kelas kelas WHERE siswa.nis = '$nis'"); 
		  #If no data was returned, check for any SQL errors 
        if (!$getnis) { 
	echo 'Could not run query: ' . mysqli_error($connection); 
           		exit;
        } 

		// 0 = Gagal insert
		// 1 = Sukses insert
		// 2 = NIS sudah ada

		// Mencek apakah NIS sudah ada di dalam database
			if (mysqli_num_rows($getnis) > 0) {
		      $response['result'] = "2";
		      $response['message'] = "Siswa sudah ada";
    		}else{
	      		try{
					$sql = "INSERT INTO `tb_siswa` ( `id_user`,`id_kelas`,`nis`,`nama_siswa`,`password`,`tahun_ajaran`,`is_absen`) VALUES ( '$iduser', '$idkelas', '$nis', '$namasiswa', '$password', '$tahunajaran', '$isabsen')";
					
					//adding the path and name to database 
					if(mysqli_query($connection,$sql)){
			
						//filling response array with values 
						$response['result'] = "1"; 
						$response['message'] = "Siswa berhasil ditambah";
					}else{
						echo 'Terjadi kesalahan, ulangi lagi!';
					}
				}catch(Exception $e){
					$response['result']="0";
					$response['message']=$e->getMessage();
				}
    		}
			
			}else{
				$response['result']="0";
				$response['message']="Data kurang lengkap";
			}
			//displaying the response 
			echo json_encode($response);
			
			//closing the connection 
			mysqli_close($connection);
	}